package cc.ppi.locadora.mvc.logica;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.CategoriaDao;
import cc.ppi.jdbc.modelo.Categoria;

public class ToAdicionarCarro implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		List<Categoria> list = new ArrayList<Categoria>();
		
		CategoriaDao dao = new CategoriaDao();
		list = dao.getLista();
		
		req.setAttribute("categorias", list);
		return "adicionarCarro.jsp";
	}

}