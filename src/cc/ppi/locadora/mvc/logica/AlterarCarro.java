package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.dao.CategoriaDao;
import cc.ppi.jdbc.modelo.Carro;

public class AlterarCarro implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		CarroDao dao = new CarroDao();
		Carro carro = new Carro();
		String renavan = req.getParameter("renavanCarro");
		String nome = req.getParameter("nome");		
		String ano = req.getParameter("ano");
		String categoriaId = req.getParameter("categorias");
		
		CategoriaDao daoc = new CategoriaDao();
		// monta um objeto carro			
		carro.setAno(Integer.parseInt(ano));
		carro.setNome(nome);
		carro.setRenavan(renavan);
		carro.setCategoria(daoc.getCategoria(Integer.parseInt(categoriaId)));
				
		dao.altera(carro);
		
		return "controller?logica=HomeADM";
	}
		
}