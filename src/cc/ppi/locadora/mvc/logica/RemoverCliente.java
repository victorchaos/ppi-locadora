package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.ClienteDao;

public class RemoverCliente implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		ClienteDao dao = new ClienteDao();
		dao.remove(req.getParameter("cpfCliente"));
		
		return "controller?logica=HomeADM";
	}

}