package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.dao.CategoriaDao;

public class ToAlterarCarro implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		CarroDao dao = new CarroDao();
		req.setAttribute("carro", dao.getCarro(req.getParameter("renavanCarro")));
		
		CategoriaDao daoc = new CategoriaDao();
		req.setAttribute("categorias", daoc.getLista());
		
		return "alterarCarro.jsp";
	}

}