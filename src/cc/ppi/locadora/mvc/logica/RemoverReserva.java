package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.AluguelDao;

public class RemoverReserva implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		AluguelDao dao = new AluguelDao();
		dao.remove(req.getParameter("reservaId"));
		
		return "controller?logica=HomeADM";
	}

}