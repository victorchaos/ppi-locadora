package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.jdbc.modelo.Aluguel;
import cc.ppi.jdbc.modelo.Carro;
import cc.ppi.jdbc.modelo.Cliente;

public class FinishRentClienteExiste  implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ClienteDao dao = new ClienteDao();
		Cliente cliente = new Cliente();		
		
		cliente = dao.getCliente(req.getParameter("cpfExistente"));
		
		if (cliente.getCpf() != null) {
			String renavan = req.getParameter("renavan");
			// fazendo a convers�o da data
					
			AluguelDao daoa = new AluguelDao();
			Aluguel aluguel = new Aluguel();
			aluguel = daoa.getAluguel(Integer.valueOf(req.getParameter("aluguelId")));
			CarroDao daoc = new CarroDao();
			Carro cr = new Carro();
			cr = daoc.getCarro(renavan);
			aluguel.setCarro(cr);
			aluguel.setCliente(cliente);		
			daoa.altera(aluguel);
		
			req.setAttribute("reserva", aluguel);
			
			return "homeCliente.jsp";
		}else {
			req.setAttribute("canrent", "Nenhum cliente cadastrado com esse CPF");
			return "controller?logica=ListarCategoria";
		}
				
	}

}
