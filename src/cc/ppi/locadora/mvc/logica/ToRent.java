package cc.ppi.locadora.mvc.logica;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.modelo.Aluguel;
import cc.ppi.jdbc.modelo.Carro;

public class ToRent implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
				
		String dataInicio = req.getParameter("data_inicio");
		String dataFim = req.getParameter("data_fim");
		
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");		
		java.time.format.DateTimeFormatter formato = java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy");
		java.time.LocalDate dataInicioFormatada = java.time.LocalDate.parse(dataInicio, formato);
		java.time.LocalDate dataFimFormatada = java.time.LocalDate.parse(dataFim, formato);
		
		LocalDate dtFim;
		LocalDate dtInicio;
		LocalDate dtInicio1 = fmt.parseLocalDate(dataInicio);		
		LocalDate dtFim1 = fmt.parseLocalDate(dataFim);		
		if (Days.daysBetween(dtInicio1, dtFim1).getDays() > 0) {
				
			List<Carro> list = new ArrayList<Carro>();
			
			CarroDao dao = new CarroDao();		
			
			Aluguel aluguel = new Aluguel();
			aluguel.setData_fim(formato.format(dataFimFormatada));
			aluguel.setData_inicio(formato.format(dataInicioFormatada));	
			
			
			AluguelDao daoa = new AluguelDao();	
			for (Carro car : dao.getListaByCatego(Integer.valueOf(req.getParameter("categorias")))) {
				boolean dispo = true;
				for (Aluguel alu : daoa.getLista(car.getRenavan())){
					dtFim = fmt.parseLocalDate(alu.getData_fim());
					dtInicio = fmt.parseLocalDate(alu.getData_inicio());
					if ((dtInicio1.isAfter(dtInicio)  && dtInicio1.isBefore(dtFim)) || (dtFim1.isBefore(dtFim) && dtFim1.isAfter(dtInicio)
							|| dtInicio1.isEqual(dtInicio) || dtInicio.equals(dtFim) || dtFim.equals(dtInicio1) || dtFim.equals(dtFim1))) {
						dispo = false;
					}
				}
				if (dispo) 
					list.add(car);			
			}
			if (!list.isEmpty()) {
				aluguel.setId(daoa.adiciona(aluguel));
				req.setAttribute("aluguel", aluguel);
				req.setAttribute("carros", list);
				req.setAttribute("canrent", "");
				return "alugueldecarro.jsp";
			}
			else {
				req.setAttribute("canrent", "Nenhum carro desta categoria dispon�vel para essa data");
				return "controller?logica=ListarCategoria";
			}
		}else {
			req.setAttribute("canrent", "Data de aluguel deve ser menor que a data de devolu��o");
			return "controller?logica=ListarCategoria";
		}	
		
		
	}

}
