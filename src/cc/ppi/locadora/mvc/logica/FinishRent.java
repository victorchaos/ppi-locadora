package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.jdbc.modelo.Aluguel;
import cc.ppi.jdbc.modelo.Carro;
import cc.ppi.jdbc.modelo.Cliente;

public class FinishRent implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ClienteDao dao = new ClienteDao();
		Cliente cliente = new Cliente();
		String cpf = req.getParameter("cpf");
		String nome = req.getParameter("nome");		
		String email = req.getParameter("email");
		String endereco = req.getParameter("endereco");
		String dataEmTexto = req.getParameter("dataNasc");
		String telefone = req.getParameter("telefone");
				
		
		
		
		// monta um objeto contato			
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setEndere�o(endereco);		
		cliente.setEmail(email);
		cliente.setNascimento(dataEmTexto);
		cliente.setTelefone(telefone);
		
		if (dao.adiciona(cliente)) {		
			String renavan = req.getParameter("renavan");
			// fazendo a convers�o da data
					
			AluguelDao daoa = new AluguelDao();
			Aluguel aluguel = new Aluguel();
			aluguel = daoa.getAluguel(Integer.valueOf(req.getParameter("aluguelId")));
			CarroDao daoc = new CarroDao();
			Carro cr = new Carro();
			cr = daoc.getCarro(renavan);
			aluguel.setCarro(cr);
			aluguel.setCliente(cliente);		
			daoa.altera(aluguel);
		
			req.setAttribute("reserva", aluguel);		
			
			return "homeCliente.jsp";		
		}else {
			req.setAttribute("canrent", "Cliente j� cadastrado com esse CPF");
			return "controller?logica=ListarCategoria";
		}
		
		
	}

}
