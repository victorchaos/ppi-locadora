package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.modelo.Aluguel;

public class ChekReserva implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
				
		String idReserva = req.getParameter("reservaId");
		
		AluguelDao dao = new AluguelDao();
		Aluguel aluguel = dao.getAluguel(Integer.parseInt(idReserva));
		if (aluguel.getId() != 0) {
			DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");	
			LocalDate dtFim = fmt.parseLocalDate(aluguel.getData_fim());	
			LocalDate dataHoje = new LocalDate();
			int diasAtrasado = Days.daysBetween(dtFim, dataHoje).getDays();
			if (diasAtrasado > 0)
				aluguel.setValor_total(diasAtrasado*1.3*aluguel.getCarro().getCategoria().getValor()+aluguel.getValor_estimado());
			req.setAttribute("reserva", aluguel);		
			return "homeCliente.jsp";	
		}else {
			req.setAttribute("rentExist", "Reserva n�o encontrada");
			return "controller?logica=ListarCategoria";	
		}
		
	}

}
