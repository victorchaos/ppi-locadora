package cc.ppi.locadora.mvc.logica;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.modelo.Aluguel;
import cc.ppi.jdbc.modelo.Carro;

public class ToAdicionarCliente implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		Carro cr = new Carro();
			
		CarroDao dao = new CarroDao();
		cr = dao.getCarro(req.getParameter("cars"));
		Aluguel aluguel = new Aluguel();
		AluguelDao daoa = new AluguelDao();
		aluguel = daoa.getAluguel(Integer.valueOf(req.getParameter("aluguelId")));
		
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");		
		DateTime dtFim = fmt.parseDateTime(aluguel.getData_fim());
		DateTime dtInicio = fmt.parseDateTime(aluguel.getData_inicio());		
		aluguel.setValor_estimado(cr.getCategoria().getValor()*Days.daysBetween(dtInicio, dtFim).getDays());
		aluguel.setValor_total(aluguel.getValor_estimado());
		
		daoa.altera(aluguel);
		
		req.setAttribute("aluguel", aluguel);
		req.setAttribute("carro", cr);
		return "adicionacliente.jsp";
	}

}