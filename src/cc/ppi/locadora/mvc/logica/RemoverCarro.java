package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.CarroDao;

public class RemoverCarro implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		CarroDao dao = new CarroDao();
		dao.remove(req.getParameter("renavanCarro"));
		
		return "controller?logica=HomeADM";
	}

}