package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.jdbc.modelo.Cliente;

public class AlterarCliente implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ClienteDao dao = new ClienteDao();
		Cliente cliente = new Cliente();
		String cpf = req.getParameter("cpfCliente");
		String nome = req.getParameter("nome");		
		String email = req.getParameter("email");
		String endereco = req.getParameter("endereco");
		String dataEmTexto = req.getParameter("dataNasc");
		String telefone = req.getParameter("telefone");
				
		java.time.format.DateTimeFormatter formato = java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy");
		java.time.LocalDate nascimentoFormatada = java.time.LocalDate.parse(dataEmTexto, formato);
		
		// monta um objeto contato			
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setEnderešo(endereco);		
		cliente.setEmail(email);
		cliente.setNascimento(formato.format(nascimentoFormatada));
		cliente.setTelefone(telefone);
				
		dao.altera(cliente);
		
		return "controller?logica=HomeADM";
	}
		
}