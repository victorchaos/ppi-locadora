package cc.ppi.locadora.mvc.logica;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.AluguelDao;
import cc.ppi.jdbc.dao.CarroDao;
import cc.ppi.jdbc.dao.ClienteDao;
import cc.ppi.jdbc.modelo.Aluguel;
import cc.ppi.jdbc.modelo.Carro;
import cc.ppi.jdbc.modelo.Cliente;

public class HomeADM implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		List<Aluguel> listReserva = new ArrayList<Aluguel>();
		
		AluguelDao dao = new AluguelDao();
		listReserva = dao.getLista();		
		
		for (int i = 0; i < listReserva.size(); i++) {			
			if (listReserva.get(i).getCarro().getRenavan() == null || listReserva.get(i).getCliente().getCpf() == null) {
				listReserva.remove(i);
			}
		}
		
		ClienteDao daoc = new ClienteDao();
		List<Cliente> listCliente = new ArrayList<Cliente>();
		listCliente = daoc.getLista();
		
		CarroDao daoca = new CarroDao();
		List<Carro> listCarro = new ArrayList<Carro>();
		listCarro = daoca.getLista();
		
		req.setAttribute("carros", listCarro);
		req.setAttribute("clientes", listCliente);
		req.setAttribute("reservas", listReserva);
		return "homeADM.jsp";
	}

}