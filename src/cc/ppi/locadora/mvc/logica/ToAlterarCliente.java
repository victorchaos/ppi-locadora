package cc.ppi.locadora.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ppi.jdbc.dao.ClienteDao;

public class ToAlterarCliente implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		ClienteDao dao = new ClienteDao();
		req.setAttribute("cliente", dao.getCliente(req.getParameter("cpfCliente")));
		
		return "alterarCliente.jsp";
	}

}