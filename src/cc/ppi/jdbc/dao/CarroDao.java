package cc.ppi.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cc.ppi.jdbc.ConnectionFactory;
import cc.ppi.jdbc.modelo.Carro;

public class CarroDao {
	// a conex�o com o banco de dados
	private Connection connection;
	
	public CarroDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public CarroDao(Connection c) {
		this.connection = c;
	}
	
	// m�todos do DAO
	
	public void adiciona(Carro dado) {
		
		String sql = "insert into carro " +
		"(renavan, nome, categoria_id, ano)" +
		" values (?,?,?,?)";
		
		try {
			// prepared statement para inserção
			PreparedStatement stmt = connection.prepareStatement(sql);
		
			// seta os valores
			stmt.setString(1, dado.getRenavan());
			stmt.setString(2, dado.getNome());
			stmt.setInt(3, dado.getCategoria().getId());
			stmt.setInt(4, dado.getAno());
		
			// executa
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}	

	public List<Carro> getLista() {
		
		try {
			List<Carro> dado = new ArrayList<Carro>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from carro");
			ResultSet rs = stmt.executeQuery();
		
			while (rs.next()) {
				// criando o objeto Contato
				Carro carro = new Carro();
				carro.setRenavan((rs.getString("renavan")));
				carro.setAno(rs.getInt("ano"));
				CategoriaDao dao = new CategoriaDao();				
				carro.setCategoria(dao.getCategoria(rs.getInt("categoria_id")));
				carro.setNome(rs.getString("nome"));
		
				// adicionando o objeto � lista
				dado.add(carro);
		
			}
		
			rs.close();
			stmt.close();
		
			return dado;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}
	
	public void altera(Carro dado) {
		String sql = "update carro set nome = ?, categoria_id = ?, ano = ? where renavan = ?";
		try {
			
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(4, dado.getRenavan());
			stmt.setString(1, dado.getNome());
			stmt.setInt(2, dado.getCategoria().getId());
			stmt.setInt(3, dado.getAno());
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public void remove(String string) {
		
		try {
			
			PreparedStatement stmt = connection.prepareStatement("delete from carro where renavan = ?");
			stmt.setString(1, string);
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Carro getCarro(String renavan) {
		try {
			PreparedStatement stmt = this.connection.prepareStatement("select * from carro where renavan = " + renavan + ";");
			ResultSet rs = stmt.executeQuery();
			Carro carro = new Carro();
			while (rs.next()) {
				// criando o objeto Contato
				
				carro.setNome(rs.getString("nome"));
				CategoriaDao dao = new CategoriaDao();				
				carro.setCategoria(dao.getCategoria(rs.getInt("categoria_id")));
				carro.setAno(rs.getInt("ano"));
				carro.setRenavan(rs.getString("renavan"));				
		
			}
		
			rs.close();
			stmt.close();
		
			return carro;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}

	public List<Carro> getListaByCatego(Integer id) {
		try {
			List<Carro> dado = new ArrayList<Carro>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from carro where categoria_id = " + id);
			ResultSet rs = stmt.executeQuery();
		
			while (rs.next()) {
				// criando o objeto Contato
				Carro carro = new Carro();
				carro.setRenavan((rs.getString("renavan")));
				carro.setAno(rs.getInt("ano"));
				CategoriaDao dao = new CategoriaDao();				
				carro.setCategoria(dao.getCategoria(rs.getInt("categoria_id")));
				carro.setNome(rs.getString("nome"));
		
				// adicionando o objeto � lista
				dado.add(carro);
		
			}
		
			rs.close();
			stmt.close();
		
			return dado;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
	}
}
