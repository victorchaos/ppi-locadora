package cc.ppi.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cc.ppi.jdbc.ConnectionFactory;
import cc.ppi.jdbc.modelo.Categoria;

public class CategoriaDao {
	// a conex�o com o banco de dados
	private Connection connection;
	
	public CategoriaDao() {
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public CategoriaDao(Connection c) {
		this.connection = c;
	}
	
	// m�todos do DAO
	
	public void adiciona(Categoria dado) {
		
		String sql = "insert into categoria " +
		"(nome, id, valor)" +
		" values (?,?,?)";
		
		try {
			// prepared statement para inserção
			PreparedStatement stmt = connection.prepareStatement(sql);
		
			// seta os valores
			stmt.setString(1, dado.getNome());
			stmt.setInt(2, dado.getId());
			stmt.setDouble(3, dado.getValor());		
		
			// executa
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}	

	public List<Categoria> getLista() {
		
		try {
			List<Categoria> dado = new ArrayList<Categoria>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from categoria");
			ResultSet rs = stmt.executeQuery();
		
			while (rs.next()) {
				// criando o objeto Contato
				Categoria categoria = new Categoria();
				categoria.setNome(rs.getString("nome"));
				categoria.setId(rs.getInt("id"));
				categoria.setValor(rs.getDouble("valor"));
		
				// adicionando o objeto � lista
				dado.add(categoria);
		
			}
		
			rs.close();
			stmt.close();
		
			return dado;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}
	
	public void altera(Categoria dado) {
		String sql = "update categoria set nome = ?, valor = ? where id = ?";
		try {
			
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, dado.getNome());
			stmt.setDouble(2, dado.getValor());
			stmt.setInt(3, dado.getId());
			
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public void remove(Categoria dado) {
		
		try {
			
			PreparedStatement stmt = connection.prepareStatement("delete from categoria where id = ?");
			stmt.setInt(1, dado.getId());
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Categoria getCategoria(int id) {
		try {
			Categoria categoria = new Categoria();
			PreparedStatement stmt = this.connection.prepareStatement("select * from categoria where id = " + id + ";");
			ResultSet rs = stmt.executeQuery();
		
			while (rs.next()) {
				categoria.setNome(rs.getString("nome"));
				categoria.setId(rs.getInt("id"));
				categoria.setValor(rs.getDouble("valor"));
		
			}
		
			rs.close();
			stmt.close();
		
			return categoria;
		
		} catch (SQLException e) {
		throw new RuntimeException(e);
		}
		
	}
	
}
