package cc.ppi.jdbc.modelo;

public class Aluguel {
	private Cliente cliente;
	private Carro carro;
	private String data_inicio;
	private String data_fim;
	private int id;
	private double valor_estimado;
	private double valor_total;

	public Aluguel() {
		
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public String getData_inicio() {
		return data_inicio;
	}

	public void setData_inicio(String data_inicio) {
		this.data_inicio = data_inicio;
	}

	public String getData_fim() {
		return data_fim;
	}

	public void setData_fim(String dataInicioFormatada) {
		this.data_fim = dataInicioFormatada;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValor_estimado() {
		return valor_estimado;
	}

	public void setValor_estimado(double valor_estimado) {
		this.valor_estimado = valor_estimado;
	}

	public double getValor_total() {
		return valor_total;
	}

	public void setValor_total(double valor_total) {
		this.valor_total = valor_total;
	}
}
