package cc.ppi.jdbc.modelo;


public class Cliente {
	private String cpf;
	private String nome;
	private String email;
	private String enderešo;
	private String nascimento;
	private String telefone;
	private String senha;
	
	public Cliente() {
		
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnderešo() {
		return enderešo;
	}

	public void setEnderešo(String enderešo) {
		this.enderešo = enderešo;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String dateTime) {
		this.nascimento = dateTime;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
