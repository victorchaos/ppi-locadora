package cc.ppi.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	public Connection getConnection() {
		
		try {
			//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			Class.forName("org.sqlite.JDBC");
			return DriverManager.getConnection("jdbc:sqlite:B:\\WorkSpace\\ppi-locadora\\data.db");
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}
