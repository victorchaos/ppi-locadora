<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reserva</title>
</head>
<style>
	*{margin : 0; padding : 0}
	input[type="text"]{width : 290px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; border:0 none; margin:15px 0 10px 0;}
	select{width : 309px; height:40px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; margin:15px 0 10px 0;}

	input[type="submit"]{width : 120px; padding: 10px; border-radius:5px; background:#008C46; color:#fff; border-radius:10px; border:0 none; margin:15px 0 10px 0; transition:all .2s linear;}
	input[type="submit"]:hover{transition:all .2s linear; transform: scale(1.1); cursor: pointer; background: #006C36;}
	html, body{

		background: #00698C;
		font-family: verdana, sans-serif;
		font-size: 14px;



	    animation-name: fadein;
	    animation-duration: 0.5s;

	}
	.container{
		margin:150px auto 0;
		width:350px;
		background: #fff;
		border-radius:20px 0 20px;
		text-align: center;
		padding:20px;
		box-shadow: 0px 0px 10px #000;


	    animation-name: topslide;
	    animation-duration: 0.5s;


	}

	@keyframes topslide {
	    from {margin-top: 10px;}
	    to {margin-top: 150px;}
	}
	@keyframes fadein {
	    from {opacity: 0; background: #fff;}
	    to {opacity: 1; background: #00698C;}
	}
</style>
<body>	
<div class="container">
	<h2>Detalhes da Reserva</h2> <hr /><br />
	<h3>Id da reserva = ${reserva.id}</h3>
	<h3>Data de Aluguel = ${reserva.data_inicio}</h3>	
	<h3>Data para entrega = ${reserva.data_fim}</h3>
	<h3>Valor sem juros = ${reserva.valor_estimado}</h3>
	<h3 id="valorComJuros">Valor total =  ${reserva.valor_total}</h3>	
	<form action="controller?logica=DevolverCarro" method="post" >
		<input type="hidden" name="reservaId" value="${reserva.id}"/>
		<input type="submit" value="Devolver" />
	</form>	
</div>
</body>
</html>