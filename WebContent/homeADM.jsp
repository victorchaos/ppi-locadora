<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Area de administração</title>
<style>
	*{margin : 0; padding : 0}
	input[type="text"]{width : 290px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; border:0 none; margin:15px 0 10px 0;}
	select{width : 309px; height:40px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; margin:15px 0 10px 0;}

	input[type="submit"]{width : 70px; padding: 10px; border-radius:1px; background:#008C46; color:#fff; border-radius:1px; border:0 none; margin:15px 0 10px 0; transition:all .2s linear;}
	input[type="submit"]:hover{transition:all .2s linear; transform: scale(1.1); cursor: pointer; background: #006C36;}
	html, body{

		background: #00698C;
		font-family: verdana, sans-serif;
		font-size: 14px;



	    animation-name: fadein;
	    animation-duration: 0.5s;

	}
	th {
	    background-color: #00698C;
	    color: white;
	    padding: 5px;
	}
	th, td {
    	border-bottom: 1px solid #ddd;
	}
	tr:hover {background-color: #f5f5f5}
	.container{
		margin:150px auto 0;
		display: table;
		background: #fff;
		border-radius:20px 0 20px;
		text-align: center;
		padding:20px;
		box-shadow: 0px 0px 10px #000;


	    animation-name: topslide;
	    animation-duration: 0.5s;


	}

	@keyframes topslide {
	    from {margin-top: 10px;}
	    to {margin-top: 150px;}
	}
	@keyframes fadein {
	    from {opacity: 0; background: #fff;}
	    to {opacity: 1; background: #00698C;}
	}
</style>
</head>
<body>
<div class="container">
	<table>
		<tr>
    		<th>ID</th>
    		<th>CPF do Cliente</th> 
    		<th>Renavan do Carro</th>
    		<th>Data de Aluguel</th>
    		<th>Data de devolução</th>
    		<th>Valor</th>
 		</tr>
 		<c:forEach var = "reserva" items="${reservas}">
		<tr>			
    		<td>${reserva.id}</td>
    		<td>${reserva.cliente.cpf}</td> 
    		<td>${reserva.carro.renavan}</td>
    		<td>${reserva.data_inicio}</td>
    		<td>${reserva.data_fim}</td>
    		<td>${reserva.valor_estimado}</td>
    		<td>
	    		<form action="controller?logica=RemoverReserva" method="post">
	    			<input type="hidden" name="reservaId" value="${reserva.id}" >
	    			<input type="submit" value="Remover">
	    		</form>
    		</td>
 		</tr>
		</c:forEach>
	</table>
</div>
<div class="container" style="margin:30px auto 0; width:600px;">
<table>
		<tr>
    		<th>CPF</th>
    		<th>Nome</th> 
    		<th>Endereço</th>
    		<th>Nascimento</th>
    		<th>Email</th>
    		<th>Telefone</th>
 		</tr>
 		<c:forEach var = "cliente" items="${clientes}">
		<tr>			
    		<td>${cliente.cpf}</td>
    		<td>${cliente.nome}</td> 
    		<td>${cliente.endereço}</td>
    		<td>${cliente.nascimento}</td>
    		<td>${cliente.email}</td>
    		<td>${cliente.telefone}</td>
    		<td>
	    		<form action="controller?logica=RemoverCliente" method="post">
	    			<input type="hidden" name="cpfCliente" value="${cliente.cpf}" >
	    			<input type="submit" value="Remover">
	    		</form>
    		</td>
    		<td>
	    		<form action="controller?logica=ToAlterarCliente" method="post">
	    			<input type="hidden" name="cpfCliente" value="${cliente.cpf}" >
	    			<input type="submit" value="Alterar">
	    		</form>
    		</td>
 		</tr>
		</c:forEach>
	</table>
</div>
<div class="container" style="margin:30px auto 0; width:410px;">
<table>
		<tr>
    		<th>Renavan</th>
    		<th>nome</th> 
    		<th>categoria</th>
    		<th><form action="controller?logica=ToAdicionarCarro" method="post">
	    			<input type="submit" value="Adicionar">
	    		</form>
	    	</th>
 		</tr>
 		<c:forEach var = "carro" items="${carros}">
		<tr>			
    		<td>${carro.renavan}</td>
    		<td>${carro.nome}</td> 
    		<td>${carro.categoria.nome}</td>
    		<td>
	    		<form action="controller?logica=RemoverCarro" method="post">
	    			<input type="hidden" name="renavanCarro" value="${carro.renavan}" >
	    			<input type="submit" value="Remover">
	    		</form>
    		</td>
    		<td>
	    		<form action="controller?logica=ToAlterarCarro" method="post">
	    			<input type="hidden" name="renavanCarro" value="${carro.renavan}" >
	    			<input type="submit" value="Alterar">
	    		</form>
    		</td>
 		</tr>
		</c:forEach>
	</table>
</div>
</body>
</html>