<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Alterar Cliente</title>
</head>
<style>
	*{margin : 0; padding : 0}
	input[type="text"]{width : 290px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; border:0 none; margin:15px 0 10px 0;}
	select{width : 309px; height:40px; padding: 10px; border-radius:5px; background:#00455B; color:#fff; border-radius:10px; margin:15px 0 10px 0;}

	input[type="submit"]{width : 120px; padding: 10px; border-radius:5px; background:#008C46; color:#fff; border-radius:10px; border:0 none; margin:15px 0 10px 0; transition:all .2s linear;}
	input[type="submit"]:hover{transition:all .2s linear; transform: scale(1.1); cursor: pointer; background: #006C36;}
	html, body{

		background: #00698C;
		font-family: verdana, sans-serif;
		font-size: 14px;



	    animation-name: fadein;
	    animation-duration: 0.5s;

	}
	.container{
		margin:150px auto 0;
		width:350px;
		background: #fff;
		border-radius:20px 0 20px;
		text-align: center;
		padding:20px;
		box-shadow: 0px 0px 10px #000;


	    animation-name: topslide;
	    animation-duration: 0.5s;


	}

	@keyframes topslide {
	    from {margin-top: 10px;}
	    to {margin-top: 150px;}
	}
	@keyframes fadein {
	    from {opacity: 0; background: #fff;}
	    to {opacity: 1; background: #00698C;}
	}
</style>
<body>
<div class="container" style="margin: 30px auto 0">
	<label>Alterar Cliente</label><br />
	<hr />
	<br />
	<form action="controller?logica=AlterarCliente" method="post" >	
		<label>CPF do Cliente:</label> <input placeholder="000.000.000-00" type="text" onkeydown="javascript: fMasc( this, mCPF );" maxlength="15" name="cpf" value="${cliente.cpf}" disabled="disabled" /><br />
		<label>Nome: </label><input type="text" name="nome" value="${cliente.nome}" /><br />
		<label>E-mail:</label> <input type="text" name="email" value="${cliente.email}" /><br />
		<label>Endereço: </label><input type="text" name="endereco" value="${cliente.endereço}" /><br />	
		<label>Data Nascimento:</label> <input type="text" placeholder="dd/mm/yyyy" onkeyup="
        var v = this.value;
        if (v.match(/^\d{2}$/) !== null) {
            this.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
            this.value = v + '/';
        }"
    	maxlength="10"  name="dataNasc" value="${cliente.nascimento}" /><br />
		<label>Telefone: </label><input type="text" placeholder="(84) 9 9999-9999" name="telefone" placeholder="(84) 9 9999-9999" onkeydown="javascript: fMasc( this, mTel );" maxlength="14" value="${cliente.telefone}" /><br />
		<input type="hidden" name="cpfCliente" value="${cliente.cpf}">
	<input type="submit" value="Alterar" />
	</form>
</div>
<script type="text/javascript">
			function fMasc(objeto,mascara) {
				obj=objeto
				masc=mascara
				setTimeout("fMascEx()",1)
			}
			function fMascEx() {
				obj.value=masc(obj.value)
			}
			function mTel(tel) {
				tel=tel.replace(/\D/g,"")
				tel=tel.replace(/^(\d)/,"($1")
				tel=tel.replace(/(.{3})(\d)/,"$1)$2")
				if(tel.length == 9) {
					tel=tel.replace(/(.{1})$/,"-$1")
				} else if (tel.length == 10) {
					tel=tel.replace(/(.{2})$/,"-$1")
				} else if (tel.length == 11) {
					tel=tel.replace(/(.{3})$/,"-$1")
				} else if (tel.length == 12) {
					tel=tel.replace(/(.{4})$/,"-$1")
				} else if (tel.length > 12) {
					tel=tel.replace(/(.{4})$/,"-$1")
				}
				return tel;
			}
			function mCNPJ(cnpj){
				cnpj=cnpj.replace(/\D/g,"")
				cnpj=cnpj.replace(/^(\d{2})(\d)/,"$1.$2")
				cnpj=cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
				cnpj=cnpj.replace(/\.(\d{3})(\d)/,".$1/$2")
				cnpj=cnpj.replace(/(\d{4})(\d)/,"$1-$2")
				return cnpj
			}
			function mCPF(cpf){
				cpf=cpf.replace(/\D/g,"")
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
				cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
				return cpf
			}
			function mCEP(cep){
				cep=cep.replace(/\D/g,"")
				cep=cep.replace(/^(\d{2})(\d)/,"$1.$2")
				cep=cep.replace(/\.(\d{3})(\d)/,".$1-$2")
				return cep
			}
			function mNum(num){
				num=num.replace(/\D/g,"")
				return num
			}
</script>
</body>
</html>